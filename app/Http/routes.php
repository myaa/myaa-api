<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/docs', function(){
    return View::make('docs.v1.index');
});

Route::group(['middleware' => 'cors', 'prefix' => 'v1'], function(\Illuminate\Routing\Router $router){


    // --- AUTHENTICATION ROUTES ---
    $router->resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    $router->post('authenticate', 'AuthenticateController@authenticate');

    // --- USER ROUTES ---
    $router->post('signup', 'UserController@signup');

    // --- SHARED ROUTES --
    // ROUTES FOR ALL ROLES
    $router->group(['middleware' => 'role:owner,writer,commenter,reader'], function(\Illuminate\Routing\Router $router)
    {
        //ROLES
        //Get all roles
        $router->get('roles', 'RoleController@getAllRoles');
        //Get role id from user
        $router->get('role/id/{user_id}', 'RoleController@getUserRoleId');
        //Get role name from user
        $router->get('role/{user_id}', 'RoleController@getUserRoleName');
        //Get role name from role id
        $router->get('role/name/{role_id}', 'RoleController@getRoleName');

        //USER
        //Get profile of currently authenticated user.
        $router->get('user/profile', 'UserController@getCurrentUserProfile');

        //FILES
        //Get a Drive file
        $router->get('file/{file_id}', 'FileController@getFile');
        //Get all Drive files
        $router->get('files', 'FileController@getAllDriveFiles');
        //Get file download
        $router->get('download/{file_id}', 'FileController@getFileDownload');

        //ABOUT
        //Get the about of the Google Drive
        $router->get('about', 'AboutController@getAbout');
    });

    // OWNER & WRITER ROUTES
    $router->group(['middleware' => 'role:owner,writer'], function(\Illuminate\Routing\Router $router)
    {
        //FILES
        //Get all available mimetypes for file creation
        $router->get('mimetypes', 'FileController@getAllAvailableMimeTypes');
        //Create a new file
        $router->post('create/file','FileController@createFile');
    });

    // OWNER ROUTES
    $router->group(['middleware' => 'role:owner'], function(\Illuminate\Routing\Router $router)
    {
        //USERS
        //Get all unverified users
        $router->get('users/unverified', 'UserController@getAllUnverifiedUsers');
        //Verify a unverified user
        $router->post('user/verify/{user_id}', 'UserController@verifyUser');
        //Delete a user
        $router->delete('user/delete/{user_id}', 'UserController@deleteUser');
        //Assign a role to a user
        $router->post('user/role/assign','RoleController@assignRoleToUser');
        //Update a user
        $router->post('user/update', 'UserController@updateUser');
    });
    
    // WRITER ROUTES
    $router->group(['middleware' => 'role:writer'], function(\Illuminate\Routing\Router $router)
    {

    });

    // COMMENTER ROUTES
    $router->group(['middleware' => 'role:commenter'], function(\Illuminate\Routing\Router $router)
    {

    });

    // READER ROUTES
    $router->group(['middleware' => 'role:reader'], function(\Illuminate\Routing\Router $router)
    {

    });
});

