<?php

namespace App\Http\Middleware;

use App\Http\Controllers\RoleController;
use App\Role;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $rolegroup)
    {
        $role_id = RoleController::getRoleIdFromRequest($request);
        $role = RoleController::getRoleNameForCorrectRouting($role_id);
        
        if($rolegroup == $role)
        {
            return $next($request);
        }

        return json_encode(['error' => 'true', 'message' => 'Unauthorized access.']);
    }
}
