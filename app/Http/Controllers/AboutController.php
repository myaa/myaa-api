<?php

namespace App\Http\Controllers;

use App\Services\Drive_Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    private function getService()
    {
        $drive_client = new Drive_Client();
        $service= $drive_client->getService();
        return $service;
    }

    /**
     * Print information about the current user along with the Drive API settings. Returns [current_service_provider, root_folder_id, total_quota, used_quota]
     *
     */
    public function getAbout() {
        try {
            $about = $this->getService()->about->get();

            return response()->json([
                'current_service_account'   => $about->getName(),
                'root_folder_id'            => $about->getRootFolderId(),
                'total_quota'               => $about->getQuotaBytesTotal(),
                'used_quota'                => $about->getQuotaBytesUsed()
            ]);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }
}
