<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maknz\Slack\Facades\Slack;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function userLogToSlack($fallback, $pretext, $color, $message, $username,$kind)
    {
        Slack::attach([
            'fallback'  => $fallback,
            'pretext'   => $pretext . ': *' . $username . '*',
            'color'     => $color,
            'fields'    => [
                [
                    'title' => 'Time: ',
                    'value' => date('Y/m/d H:i:s', time())
                ],
                [
                    'title' => 'Message: ',
                    'value' => $message
                ],
            ],
            'mrkdwn_in' => ['pretext', 'text']
        ])->send($kind);
    }
}
