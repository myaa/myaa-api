<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;

class TokenController extends Controller
{
    public static function getTokenPayload(Request $request)
    {
        $token = $request->header('Authorization');
        $payload = JWTAuth::getPayload(substr($token,6));
        return $payload;
    }
}
