<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maknz\Slack\Facades\Slack;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class UserController extends Controller
{
    /**
     * Register a new user by form post [email, name, password].
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request)
    {
        try {
            $this->validate($request, [
                'email'     => 'required|unique:users|max:50',
                'name'      => 'required|max:30',
                'password'  => 'required|min:8'
            ]);

            User::create([
                'email'     =>  $request->input('email'),
                'name'      =>  $request->input('name'),
                'password'  =>  bcrypt($request->input('password')),
                'role_id'   =>  null
            ]);

            LogController::userLogToSlack('The user was successfully registered', 'Deleted user', 'danger', 'A user successfully registered.',$request->only('name'), '[USER LOGGING]');

            return response()->json(['error' => 'false', 'message' => 'Registered ' . $request->input('name') . ' as a user.']);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Get profile of currently authenticated user. Returns following ['id', 'name', 'email', 'role_id', 'created_at', 'updated_at']
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentUserProfile()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // The token is valid and we have found the user via the Sub Claim
        return response()->json(compact('user'));
    }

    /**
     * Get all unverified users. Returns array following [['id', 'name', 'email', 'role_id', 'created_at', 'updated_at']]
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getAllUnverifiedUsers()
    {
        try {
            $users = User::where(['role_id' => null])->get();

            return response()->json([
                'error'     =>  'false',
                'message'   =>  'Get all unverified users',
                'users'     =>  $users
            ]);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Update the specified user by form post['user_id','name','email'].
     *
     * @see name, email, user_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request)
    {
        try {
            $userToUpdate = User::findOrFail($request->input('user_id'));
            $userToUpdate->name = $request->input('name');
            $userToUpdate->name = $request->input('email');
            $userToUpdate->save();
            LogController::userLogToSlack('The user was successfully updated', 'Updated user', 'danger', 'The user was successfully updated.',$request->input('name'), '[USER LOGGING]');
            return response()->json(['error' => 'false', 'message' => 'Updated ' . $request->input('name') . '.']);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Remove the specified user by user_id. Returns ['error', 'message']
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteUser($user_id)
    {
        try {
            $user = User::findOrFail($user_id);

            //save username in variable for JSON response
            $username = $user->name;
            $user->delete();

            LogController::userLogToSlack('The user was successfully deleted', 'Deleted user', 'danger', 'The user was successfully deleted.',$username, '[USER LOGGING]');

            return response()->json([
                'error'     =>  'false',
                'message'   =>  'Successfully deleted user: ' . $username
            ]);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Verify a unverified user
     *
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyUser($user_id)
    {
        try {
            $user = User::where(['id' => $user_id])->first();
            $role_id = 4;
            $role_name = Role::findOrFail($role_id)->name;
            if($user->role_id == null) {
                $user->role_id = $role_id;
                $user->save();

                return response()->json([
                    'error' => 'false',
                    'message' => 'Verified user: ' . $user->name . ' as a ' . $role_name,
                    'user' => $user
                ]);
            }
            else
            {
                return response()->json([
                    'error' => 'true',
                    'message' => 'Something went wrong while assigning the role to the user']);
            }
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }
}
