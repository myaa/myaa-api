<?php

namespace App\Http\Controllers;

use App\File;
use App\Services\Drive_Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    private function getService()
    {
        $drive_client = new Drive_Client();
        $service= $drive_client->getService();
        return $service;
    }

    /**
     * Create a file by form post. Post parameters [title, description, 'parent_id', 'mimetype', 'filename']
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFile(Request $request)
    {
        try {
            $title          =   $request->input('title');
            $description    =   $request->input('description');
            $parentId       =   $request->input('parent_id');
            $mimeType       =   $request->input('mimetype');
            $filename       =   $request->input('filename');

            File::insertFile($this->getService(), $title, $description, $parentId, $mimeType, $filename);
            return response()->json(['error' => 'true', 'message' => 'Created file in Google Drive']);
        } 
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Get file by file_id. Returns ['file_id', 'title', 'mime_type, 'description']
     *
     * @param $file_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFile($file_id)
    {
        try{
            $file = File::retrieveFile($this->getService(), $file_id);
            return response()->json([
                'error' => 'false',
                'message' => 'Get file',
                [
                    'file_id' => $file_id,
                    'title' => $file->getTitle(),
                    'mime_type' => $file->getMimeType(),
                    'description' => $file->getDescription()
                ]
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: '. $e->getMessage()]);
        }
    }

    /**
     * Get all files in Drive.  Returns an array with all files following ['file_id', 'file_title']
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDriveFiles()
    {
        try {
            $files = File::retrieveAllDriveFiles($this->getService());
            $file_collection = collect($files);
            $collection_to_array = [];
            foreach($file_collection->all() as $file)
            {
                array_push($collection_to_array, ['file_id' => $file->id, 'file_title' => $file->title]);
            }
            return response()->json(['error' => 'false', 'message' => 'Get all files', 'files' => $collection_to_array]);
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Get file download.
     *
     * @param $file_id
     * @return \Illuminate\Http\JsonResponse|String
     */
    public function getFileDownload($file_id)
    {
        try{
            $file = File::downloadFile($this->getService(), $file_id);
            return $file;
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: '. $e->getMessage()]);
        }
    }

    /**
     * Get all available mimetypes for file creation.  Returns an array following ['mime_types']
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllAvailableMimeTypes()
    {
        $mimeTypes = File::retrieveAllSupportedMimeTypes();
        return response()->json(['error' => 'false', 'message' => 'Get all mime types for file creation', 'mime_types' => $mimeTypes]);
    }
}
