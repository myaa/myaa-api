<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;

class AuthenticateController extends Controller
{
    /**
     * AuthenticateController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    
    /**
     * Return a JWT
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        //Get credentials from request
        $credentials = $request->only('email', 'password');

        //Get user from database or fail
        $user = User::where(['email' => $request->only('email')])->firstOrFail();
        
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials,["role_id"=>$user->role_id])) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT TOKEN to React
        return response()->json(compact('token'));
    }
}
