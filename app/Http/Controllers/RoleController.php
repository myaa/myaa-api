<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use JWTAuth;
use App\Http\Controllers\TokenController;

class RoleController extends Controller
{
    public static function getRoleIdFromRequest(Request $request)
    {
        try {
            $role_id = TokenController::getTokenPayload($request)->getClaims()[0]->getValue();
            return $role_id;
        }
        catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    public static function getRoleNameForCorrectRouting($role_id)
    {
        try {
            return Role::findOrFail($role_id)->name;
        } catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occurred: ' . $e->getMessage()]);
        }
    }

    /**
     * Get the role id of a User. Returns ['user_role_id']
     *
     * @param Request $request
     * @return mixed
     */
    public static function getUserRoleId($user_id)
    {
        try
        {
            $role_id = User::findOrFail($user_id)->role_id;
            return response()->json(['error' => 'false', 'message' => 'Get role id', 'user_role_id' => $role_id]);
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }

    /**
     * Get the role name of a User by user_id. Returns ['user_role_name']
     *
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getUserRoleName($user_id)
    {
        try
        {
            $role_name = Role::findOrFail(User::findOrFail($user_id)->role_id)->name;
            return response()->json(['error' => 'false', 'message' => 'Get role name', 'user_role_name' => $role_name]);
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }

    /**
     * Get role name by role_id. Returns ['role_name'].
     *
     * @param $role_id
     * @return mixed
     */
    public static function getRoleName($role_id)
    {
        try
        {
            $role_name = Role::findOrFail($role_id)->name;
            return response()->json(['error' => 'false', 'message' => 'Get role name', 'role_name' => $role_name]);
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }

    /**
     * Assign a role to a user by form post. Post parameters [user_id, role_id].
     *
     * @param Request $request
     * @return mixed
     */
    public function assignRoleToUser(Request $request)
    {
        try
        {
            $user = User::findOrFail($request->input('user_id'));
            $role_id = $request->input('role_id');
            $role_name = Role::findOrFail($role_id)->name;
            if($user->role_id == null) {
                $user->role_id = $role_id;
                $user->save();

                return response()->json([
                    'error' => 'false',
                    'message' => 'Assigned the role of: ' . $role_name . ' to ' . $user->name,
                    'user' => $user
                ]);
            }
            elseif(!$user->role == null)
            {
                $user->role = $role_id;
                $user->save();

                return response()->json([
                    'error' => 'false',
                    'message' => 'Assigned the role of: ' . $role_name . ' to ' . $user->name,
                    'user' => $user
                ]);
            }
            else
            {
                return response()->json([
                    'error' => 'true',
                    'message' => 'Something went wrong while assigning the role to the user']);
            }
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }

    /**
     * Get all roles from API. Returns array following ['roles' => ['role_id', 'role_name']
     *
     * @return mixed
     */
    public static function getAllRoles()
    {
        try
        {
            $roles = [];
            foreach(Role::all() as $role)
            {
                $role_array = ['role_id' => $role->id, 'role_name' => $role->name];
                array_push($roles, $role_array);
            }
            return response()->json([
                'error'     =>  'false',
                'message'   =>  'All roles',
                'roles'     =>  $roles
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }
}
