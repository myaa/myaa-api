<?php namespace App\Services;

use Illuminate\Support\Facades\Cache;

class Drive_Client {

    protected $client;

    protected $service;

    /**
     * @return \Google_Service_Drive
     */
    public function getService()
    {
        return $this->service;
    }

    function __construct() {
        /* Get config variables */
        $client_id = config('google.client_id');
        $service_account_name = config('google.service_account_name');
        $key_file_location = base_path() . config('google.key_file_location');

        $this->client = new \Google_Client();
        $this->client->setApplicationName("MYAA-API");
        $this->service = new \Google_Service_Drive($this->client);

        /* If we have an access token */
        if (Cache::has('service_token')) {
            $this->client->setAccessToken(Cache::get('service_token'));
        }

        $key = file_get_contents($key_file_location);

        /* Add the scopes you need */
        $scopes = array('https://www.googleapis.com/auth/drive');
        $cred = new \Google_Auth_AssertionCredentials(
            $service_account_name,
            $scopes,
            $key
        );

        $this->client->setAssertionCredentials($cred);

        if ($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
        Cache::forever('service_token', $this->client->getAccessToken());
    }
}