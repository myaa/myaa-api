<?php

namespace App;

use App\Services\Drive_Client;

class File
{
    /**
     * Insert new file.
     *
     * @param Google_Service_Drive $service Drive API service instance.
     * @param string $title Title of the file to insert, including the extension.
     * @param string $description Description of the file to insert.
     * @param string $parentId Parent folder's ID.
     * @param string $mimeType MIME type of the file to insert.
     * @param string $filename Filename of the file to insert.
     * @return Google_Service_Drive_DriveFile The file that was inserted. NULL is
     *     returned if an API error occurred.
     */
    public static function insertFile($service, $title, $description, $parentId, $mimeType, $filename) {
        $file = new \Google_Service_Drive_DriveFile();
        $file->setTitle($title);
        $file->setDescription($description);
        $file->setMimeType($mimeType);

        // Set the parent folder.
        if ($parentId != null) {
            $parent = new \Google_Service_Drive_ParentReference();
            $parent->setId($parentId);
            $file->setParents(array($parent));
        }

        try {
            $data = file_get_contents($filename);

            $createdFile = $service->files->insert($file, array(
                'data' => $data,
                'mimeType' => $mimeType,
            ));

            return $createdFile;
        } catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }

    static function retrieveFile($service, $fileId) {
        try {
            $file = $service->files->get($fileId);
            return $file;
        } catch (\Exception $e) {
            return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
        }
    }

    /**
     * Download a file's content.
     *
     * @param Google_Service_Drive $service Drive API service instance.
     * @param File $file Drive File instance.
     * @return String The file's content if successful, null otherwise.
     */
    static function downloadFile($service, $file_id) {
        $file = self::retrieveFile($service, $file_id);
        $downloadUrl = $file->getDownloadUrl();
        if ($downloadUrl) {
            $request = new \Google_Http_Request($downloadUrl, 'GET', null, null);
            $httpRequest = $service->getClient()->getAuth()->authenticatedRequest($request);
            if ($httpRequest->getResponseHttpCode() == 200) {
                return $httpRequest->getResponseBody();
            } else {
                // An error occurred.
                return response()->json(['error' => 'true', 'message' => 'An error occurred.']);
            }
        } else {
            // The file doesn't have any content stored on Drive.
            return response()->json(['error' => 'true', 'message' => "The file doesn't have any content stored on Drive."]);
        }
    }

    static function retrieveAllDriveFiles($service) {
        $result = array();
        $pageToken = NULL;

        do {
            try {
                $parameters = array();
                if ($pageToken) {
                    $parameters['pageToken'] = $pageToken;
                }
                $files = $service->files->listFiles($parameters);

                $result = array_merge($result, $files->getItems());
                $pageToken = $files->getNextPageToken();
            } catch (\Exception $e) {
                $pageToken = NULL;
                return response()->json(['error' => 'true', 'message' => 'An error occured: '. $e->getMessage()]);
            }
        } while ($pageToken);
        return $result;
    }

    public static function retrieveAllSupportedMimeTypes() {
        $mimeTypes = [
            'audio'                 =>  'application/vnd.google-apps.audio',
            'unknown'               =>  'application/vnd.google-apps.unknown',
            'video'                 =>  'application/vnd.google-apps.video',
            'photo'                 =>  'application/vnd.google-apps.photo',
            'google_doc'            =>  'application/vnd.google-apps.document',
            'google_drawing'        =>  'application/vnd.google-apps.drawing',
            'google_drive_file'     =>  'application/vnd.google-apps.file',
            'google_drive_folder'   =>  'application/vnd.google-apps.folder',
            'google_form'           =>  'application/vnd.google-apps.form',
            'google_map'            =>  'application/vnd.google-apps.map',
            'google_slide'          =>  'application/vnd.google-apps.presentation',
            'google_sheet'          =>  'application/vnd.google-apps.spreadsheet',
        ];
        return $mimeTypes;
    }
}
