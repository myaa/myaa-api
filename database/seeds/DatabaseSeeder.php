<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $roles = array(
            ['name' => 'owner'],
            ['name' => 'writer'],
            ['name' => 'commenter'],
            ['name' => 'reader']
        );

        foreach($roles as $role)
        {
            \App\Role::create($role);
        }

        $users = array(
            ['name' => 'Evert Arnould', 'email' => 'contact@evertarnould.be', 'password' => Hash::make('secret'), 'role_id' => 1],
            ['name' => 'Jetmir Shatri', 'email' => 'contact@jetmirshatri.com', 'password' => Hash::make('M1loh1o1'), 'role_id' => 1],
            ['name' => 'Jef Inghelbrecht', 'email' => 'jef.inghelbrecht@inantwerpen.be', 'password' => Hash::make('secret'), 'role_id' => 1]
        );

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            \App\User::create($user);
        }

        Model::reguard();
    }
}

